Source: python-pytest-random-order
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Francesco Ballarin <francesco.ballarin@unicatt.it>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-py,
 python3-pytest,
 python3-pytest-xdist,
 python3-setuptools,
Standards-Version: 4.6.2
Homepage: https://github.com/jbasko/pytest-random-order
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-pytest-random-order
Vcs-Git: https://salsa.debian.org/python-team/packages/python-pytest-random-order.git
Rules-Requires-Root: no

Package: python3-pytest-random-order
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: pytest plugin to randomise the order of tests (Python 3)
 pytest-random-order is a pytest plugin that randomises the order of tests.
 This can be useful to detect a test that passes just because it happens to
 run after an unrelated test that leaves the system in a favourable state.
 .
 The plugin allows the user to control the level of randomness they want to
 introduce and to disable reordering on subsets of tests. Tests can be rerun
 in a specific order by passing a seed value reported in a previous test run.
 .
 This package installs the pytest plugin for Python 3.
